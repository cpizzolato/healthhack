import numpy as np
import pandas as pd
import tarfile
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation
plt.style.use('dark_background')

colors = ['#f9ff00',  '#00f6ff', '#1fff00', '#ff0055']


def axisEqual3D(ax):
    extents = np.array([getattr(ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
    sz = extents[:,1] - extents[:,0]
    centers = np.mean(extents, axis=1)
    maxsize = max(abs(sz))
    r = maxsize/2
    for ctr, dim in zip(centers, 'xyz'):
        getattr(ax, 'set_{}lim'.format(dim))(ctr - r, ctr + r)

def plotPointCloud(filename, indeces):
    nodes = pd.read_csv(filename,delimiter=',', dtype=np.float64)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d', aspect='equal')
    ax.scatter(nodes.iloc[:,0], nodes.iloc[:,1], nodes.iloc[:,2], s=0.1,
                alpha=0.5, c='white')
    for e, i in enumerate(indeces):
        ax.scatter(*nodes.iloc[i,:], c=colors[e], s=50)
    axisEqual3D(ax)
    ax.set_axis_off()
    return ax
    
def plotStrains(filename, indeces, nrows=1000):
    data = pd.read_csv(filename,nrows=nrows, delimiter=',', dtype=np.float64) 
    timeVec = np.arange(len(data.iloc[:,0].values))*0.005;
    vonMisesStrains = np.matrix(data.iloc[:,].values)
    fig = plt.figure()                              
    ax = fig.add_subplot(111)    
    data_to_use = (timeVec, vonMisesStrains[:,indeces]*100.)    
    lines = []
    ax.set_ylabel('% strain ')
    ax.set_xlabel('time (s) ')
    for e, p in enumerate(indeces):
       line, = ax.plot([], [], lw=2, c=colors[e])
       lines.append(line)
    xdata, ydata = [], []
    ax.set_ylim([0, 6])
    
    def data_gen(cnt):
        return (data_to_use[0][cnt], 
                np.asarray(data_to_use[1][cnt]).flatten())
    
    def run(idx):
        # update the data
        t, y = data_gen(idx)
        xdata.append(t)
        ydata.append(y)
        xmin, xmax = ax.get_xlim()
        for e, l in enumerate(lines):
            l.set_data(xdata, list(zip(*ydata))[e])
        if t >= xmax:
            ax.set_xlim(xmin, 2*xmax)
            ax.figure.canvas.draw()

            
    ani = animation.FuncAnimation(fig, func=run, frames=np.arange(0,len(timeVec)), 
                                  blit=False, interval=5, repeat=False)
    return ani

path = 'data'
tar = tarfile.open('data.tar.gz')
tar.extractall()
nodesFilename = path+'/nodes.csv'
strainsFilename = path+'/strains.csv'
nodeIndeces=[85,625,1929]
plotPointCloud(nodesFilename, nodeIndeces)
ani = plotStrains(strainsFilename, nodeIndeces,nrows=5000)
plt.show()

